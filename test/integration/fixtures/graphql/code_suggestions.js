export const duoCodeSuggestionsAvailableResponse = {
  currentUser: {
    duoCodeSuggestionsAvailable: true,
  },
};
