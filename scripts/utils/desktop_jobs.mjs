import path from 'node:path';
import fs from 'node:fs';
import { copyFile } from 'node:fs/promises';
import { ENVIRONMENTS } from '../constants.mjs';
import { root, run } from './run_utils.mjs';
import { createDesktopPackageJson } from './packages.mjs';
import {
  prepareWebviews,
  generateAssets,
  writePackageJson,
  commonJobs,
  copyStaticProjectFiles,
} from './common_jobs.mjs';

const desktopWebviews = {
  vue3: ['issuable'],
  vue2: ['gitlab_duo_chat', 'security_finding'],
};

export async function copyPendingJobAssets() {
  return copyFile(
    path.join(root, 'webviews/pendingjob.html'),
    path.join(root, `dist-desktop/webviews/pendingjob.html`),
  );
}

export async function compileSource() {
  await run('tsc', ['-p', root]);
}

export async function buildExtension(args = [], signal) {
  await run(
    'esbuild',
    [
      path.join(root, 'src/desktop/extension.js'),
      '--bundle',
      '--outfile=dist-desktop/extension.js',
      '--external:vscode',
      '--platform=node',
      '--target=node18.17',
      '--sourcemap',
      '--loader:.html=text',
      ...args,
    ],
    { cancelSignal: signal },
  );
}

export async function checkAndBuildExtension(args = []) {
  await compileSource();
  await buildExtension(args);
}

export async function copyNodeModules() {
  await run('cp', ['-R', path.join(root, 'node_modules'), path.join(root, 'dist-desktop')]);
}

export async function copyWalkthroughs() {
  await run('cp', ['-R', path.join(root, 'walkthroughs'), path.join(root, 'dist-desktop')]);
}

export function watchWebviews(signal) {
  const targets = Object.keys(desktopWebviews);
  targets.forEach(async target => {
    desktopWebviews[target].forEach(webview => {
      const dirpath = path.join(root, `webviews/${target}/dist/${webview}`);
      if (!fs.existsSync(dirpath)) fs.mkdirSync(dirpath, { recursive: true });
      fs.symlinkSync(dirpath, path.join(root, `dist-desktop/webviews/${webview}`));
    });
    await run('npm', ['run', '--prefix', path.join(root, `webviews/${target}`), 'watch'], {
      cancelSignal: signal,
    });
  });
}

export async function buildDesktop() {
  const packageJson = createDesktopPackageJson();

  await commonJobs(ENVIRONMENTS.DESKTOP);

  await Promise.all([
    prepareWebviews(desktopWebviews, ENVIRONMENTS.DESKTOP),
    copyPendingJobAssets(),
    copyStaticProjectFiles(ENVIRONMENTS.DESKTOP),
    copyWalkthroughs(),
    checkAndBuildExtension(['--minify']),
    generateAssets(packageJson, ENVIRONMENTS.DESKTOP),
  ]);

  // we need to wait for `tsc` to compile so we can replace package.json
  await writePackageJson(packageJson, ENVIRONMENTS.DESKTOP);
}

export async function watchDesktop(signal) {
  const packageJson = createDesktopPackageJson();
  await compileSource();
  await writePackageJson(packageJson, ENVIRONMENTS.DESKTOP);
  copyStaticProjectFiles(ENVIRONMENTS.DESKTOP);
  copyWalkthroughs();
  await buildExtension(['--watch'], signal);
}

export async function buildPackage(options, isPreReleaseBuild) {
  const args = ['--no-dependencies'];
  if (isPreReleaseBuild) {
    args.push('--pre-release');
  }
  await run('vsce', ['package', ...args], options);
}
