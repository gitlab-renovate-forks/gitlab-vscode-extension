import vscode from 'vscode';
import { CONFIG_NAMESPACE, IS_OSX } from '../constants';
import { COMMENT_CONTROLLER_ID } from './utils';

export class QuickChatDecoration {
  #keybindingHintDecoration: vscode.TextEditorDecorationType | null = null;

  #gutterIconDecoration: vscode.TextEditorDecorationType | null = null;

  #isKeybindingHintEnabled: boolean;

  #extensionContext: vscode.ExtensionContext;

  constructor(context: vscode.ExtensionContext) {
    this.#extensionContext = context;
    this.#isKeybindingHintEnabled =
      vscode.workspace.getConfiguration(CONFIG_NAMESPACE)?.keybindingHints?.enabled || false;
  }

  updateKeybindingHint(editor: vscode.TextEditor) {
    this.#keybindingHintDecoration?.dispose();

    const position = editor.selection.active;
    const currentLine = editor.document.lineAt(position.line);
    const isFile = editor.document.uri.scheme === 'file';

    if (!this.#isKeybindingHintEnabled || !currentLine?.isEmptyOrWhitespace || !isFile) return;

    this.#keybindingHintDecoration = this.#createKeybindingHintDecoration();
    editor.setDecorations(this.#keybindingHintDecoration, [editor.selection]);
  }

  toggleGutterIcon(thread: vscode.CommentThread | null) {
    this.resetGutterIcon();
    const threadUri = thread?.uri;
    const editor = vscode.window.activeTextEditor;

    const isInCommentInput = editor?.document.uri.authority === COMMENT_CONTROLLER_ID;
    const isCollapsed = thread?.collapsibleState === vscode.CommentThreadCollapsibleState.Collapsed;

    // If we're in comment input, find the editor for the thread's document
    const targetEditor = isInCommentInput
      ? vscode.window.visibleTextEditors.find(e => e.document.uri === threadUri)
      : editor;

    const shouldShowIcon = targetEditor && !isCollapsed && targetEditor.document.uri === threadUri;

    if (!shouldShowIcon || !thread) return;

    this.updateGutterIcon(targetEditor, thread.range.end);
  }

  updateGutterIcon(editor: vscode.TextEditor, iconPosition: vscode.Position) {
    this.resetGutterIcon();

    this.#gutterIconDecoration = this.#createGutterIconDecoration(
      this.#extensionContext.extensionUri,
    );

    editor.setDecorations(this.#gutterIconDecoration, [
      {
        range: new vscode.Range(iconPosition, iconPosition),
      },
    ]);
  }

  resetGutterIcon() {
    this.#gutterIconDecoration?.dispose();
  }

  #createKeybindingHintDecoration = () =>
    vscode.window.createTextEditorDecorationType({
      after: {
        contentText: IS_OSX ? '⌥C Duo Quick Chat' : '(Alt+C) Duo Quick Chat',
        margin: '0 0 0 2ch', // Add left margin
        color: new vscode.ThemeColor('editorHint.foreground'),
        fontStyle: 'normal',
        textDecoration: 'none; filter: opacity(0.34);',
      },
    });

  #createGutterIconDecoration = (extensionUri: vscode.Uri): vscode.TextEditorDecorationType =>
    vscode.window.createTextEditorDecorationType({
      gutterIconPath: vscode.Uri.joinPath(extensionUri, 'assets/icons/gitlab-duo-quick-chat.svg'),
    });

  onConfigChange() {
    this.#isKeybindingHintEnabled =
      vscode.workspace.getConfiguration(CONFIG_NAMESPACE)?.keybindingHints?.enabled;
  }

  dispose() {
    this.#keybindingHintDecoration?.dispose();
    this.#gutterIconDecoration?.dispose();
  }
}
