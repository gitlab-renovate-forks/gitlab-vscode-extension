import vscode from 'vscode';
import { Cable } from '@anycable/core';
import { createMockTextDocument } from '../__mocks__/mock_text_document';
import { SPECIAL_MESSAGES } from '../chat/constants';
import { GitLabPlatformManagerForChat } from '../chat/get_platform_manager_for_chat';
import { AIContextManager } from '../chat/ai_context_manager';
import { createFakePartial } from '../test_utils/create_fake_partial';
import { GitLabChatApi } from '../chat/gitlab_chat_api';
import { asMutable } from '../test_utils/types';
import {
  createFakeWorkspaceConfiguration,
  createConfigurationChangeTrigger,
} from '../test_utils/vscode_fakes';
import { QuickChat, QuickChatOpenOptions, QuickChatOpenOptionsWithSelection } from './quick_chat';
import {
  COMMAND_CLOSE_QUICK_CHAT,
  COMMAND_OPEN_QUICK_CHAT,
  COMMAND_OPEN_QUICK_CHAT_WITH_SHORTCUT,
  COMMAND_QUICK_CHAT_MESSAGE_TELEMETRY,
  COMMAND_QUICK_CHAT_OPEN_TELEMETRY,
  COMMAND_SEND_QUICK_CHAT,
  COMMAND_SEND_QUICK_CHAT_DUPLICATE,
  COMMAND_SHOW_AND_SEND_QUICK_CHAT_WITH_CONTEXT,
  QUICK_CHAT_OPEN_TRIGGER,
} from './constants';
import * as utils from './utils';
import { COMMENT_CONTROLLER_ID, generateThreadLabel } from './utils';

jest.mock('../chat/gitlab_chat_api');

const mockCommentController = createFakePartial<vscode.CommentController>({
  createCommentThread: jest.fn(),
  dispose: jest.fn(),
});

jest.mock('./utils', () => {
  const originalModule = jest.requireActual('./utils');
  const mockModule = jest.createMockFromModule<typeof originalModule>('./utils');

  return {
    ...mockModule,
    MarkdownProcessorPipeline: jest.fn().mockImplementation(() => ({
      process: jest.fn(md => md),
    })),
    createCommentController: jest.fn(() => mockCommentController),
    generateThreadLabel: jest.fn(() => 'Duo Quick Chat'),
  };
});

const mockDebug = jest.fn();

jest.mock('../log', () => {
  const originalModule = jest.requireActual('../log');
  const mockModule = jest.createMockFromModule<typeof originalModule>('../log');

  return {
    ...mockModule,
    log: {
      debug: jest.fn().mockImplementation(msg => mockDebug(msg)),
      log: jest.fn(),
      error: jest.fn(),
      warn: jest.fn(),
    },
  };
});

describe('QuickChat', () => {
  let quickChat: QuickChat;
  let mockManager: GitLabPlatformManagerForChat;
  let mockApi: GitLabChatApi;
  let mockConfig: vscode.WorkspaceConfiguration;
  let mockThread: vscode.CommentThread;
  let mockAiContextManager: AIContextManager;
  let mockExtensionContext: vscode.ExtensionContext;
  let mockUri: vscode.Uri;
  let createChat = () => {};

  beforeEach(() => {
    mockUri = vscode.Uri.file('/path/to/file.ts');
    mockManager = createFakePartial<GitLabPlatformManagerForChat>({
      getProjectGqlId: jest.fn().mockResolvedValue('mock-project-id'),
      getGitLabPlatform: jest.fn().mockResolvedValue(undefined),
      getGitLabEnvironment: jest.fn().mockResolvedValue(undefined),
    });

    mockApi = createFakePartial<GitLabChatApi>({
      clearChat: jest.fn().mockResolvedValue({}),
      resetChat: jest.fn().mockResolvedValue({}),
      subscribeToUpdates: jest.fn(),
      processNewUserPrompt: jest.fn(),
    });

    (GitLabChatApi as jest.MockedClass<typeof GitLabChatApi>).mockImplementation(() => mockApi);

    mockConfig = createFakeWorkspaceConfiguration({
      keybindingHints: { enabled: true },
    });
    jest.spyOn(vscode.workspace, 'getConfiguration').mockReturnValue(mockConfig);

    mockThread = createFakePartial<vscode.CommentThread>({
      uri: mockUri,
      comments: [],
      range: new vscode.Range(0, 0, 0, 0),
    });
    mockAiContextManager = createFakePartial<AIContextManager>({});
    mockExtensionContext = createFakePartial<vscode.ExtensionContext>({});
    createChat = () => {
      quickChat = new QuickChat(mockManager, mockExtensionContext, mockAiContextManager);
    };
    createChat();
  });

  const triggerTextEditorSelectionChange = (e: vscode.TextEditorSelectionChangeEvent) => {
    jest
      .mocked(vscode.window.onDidChangeTextEditorSelection)
      .mock.calls.forEach(([listener]) => listener(e));
  };

  async function showChatWithEditor(userInput = 'Test question') {
    const cursorPosition = new vscode.Position(1, 11);
    const mockRange = new vscode.Range(new vscode.Position(1, 0), cursorPosition);
    const mockDocument = createMockTextDocument({
      uri: mockUri,
      content: 'full document content\nsecond line\nthird line',
    });
    const mockEditor = createFakePartial<vscode.TextEditor>({
      document: mockDocument,
      selection: {
        ...mockRange,
        active: cursorPosition,
      },
      setDecorations: jest.fn(),
    });
    asMutable(vscode.window).activeTextEditor = mockEditor;
    asMutable(vscode.workspace).asRelativePath = jest.fn().mockReturnValue(mockUri.path);

    (mockCommentController.createCommentThread as jest.Mock).mockReturnValue(mockThread);

    jest.spyOn(vscode.window, 'showInputBox').mockResolvedValue(userInput);

    await quickChat.show();

    return { mockRange, mockEditor };
  }

  it('registers commands', () => {
    expect(vscode.commands.registerCommand).toHaveBeenCalledWith(
      COMMAND_OPEN_QUICK_CHAT,
      expect.any(Function),
    );
    expect(vscode.commands.registerCommand).toHaveBeenCalledWith(
      COMMAND_SEND_QUICK_CHAT,
      expect.any(Function),
    );
    expect(vscode.commands.registerCommand).toHaveBeenCalledWith(
      COMMAND_SEND_QUICK_CHAT_DUPLICATE,
      expect.any(Function),
    );
    expect(vscode.commands.registerCommand).toHaveBeenCalledWith(
      COMMAND_OPEN_QUICK_CHAT_WITH_SHORTCUT,
      expect.any(Function),
    );
    expect(vscode.commands.registerCommand).toHaveBeenCalledWith(
      COMMAND_CLOSE_QUICK_CHAT,
      expect.any(Function),
    );
    expect(vscode.commands.registerCommand).toHaveBeenCalledWith(
      COMMAND_SHOW_AND_SEND_QUICK_CHAT_WITH_CONTEXT,
      expect.any(Function),
    );
    expect(vscode.commands.registerCommand).toHaveBeenCalledTimes(6);
  });

  it('disposes commands', () => {
    const disposeHandler = jest.fn();
    jest.mocked(vscode.commands.registerCommand).mockReturnValue({ dispose: disposeHandler });
    quickChat = new QuickChat(mockManager, mockExtensionContext, mockAiContextManager);
    quickChat.dispose();
    expect(disposeHandler).toHaveBeenCalledTimes(6);
  });

  describe('show', () => {
    describe('with no open options', () => {
      it('creates a new comment thread when an editor is active', async () => {
        const { mockRange } = await showChatWithEditor();

        expect(mockApi.clearChat).toHaveBeenCalled();
        expect(utils.setLoadingContext).toHaveBeenCalledWith(false);
        expect(mockCommentController.createCommentThread).toHaveBeenCalledWith(
          mockUri,
          mockRange,
          [],
        );
        expect(mockThread.collapsibleState).toBe(vscode.CommentThreadCollapsibleState.Expanded);
        expect(mockThread.label).toBe('Duo Quick Chat');
      });

      it('does nothing when no editor is active', async () => {
        asMutable(vscode.window).activeTextEditor = undefined;

        await quickChat.show();

        expect(mockApi.clearChat).not.toHaveBeenCalled();
      });

      it('sends user input when provided through input box', async () => {
        jest.spyOn(quickChat, 'send');
        const userInput = 'This is a test question';
        await showChatWithEditor(userInput);

        expect(vscode.window.showInputBox).toHaveBeenCalledWith({
          placeHolder: 'Ask a question or give an instruction...',
          title: 'GitLab Duo Quick Chat',
        });

        expect(quickChat.send).toHaveBeenCalledWith(
          { text: userInput, thread: mockThread },
          undefined,
        );
      });

      it('tracks telemetry event for chat open', async () => {
        await showChatWithEditor();

        expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
          COMMAND_QUICK_CHAT_OPEN_TELEMETRY,
          { trigger: QUICK_CHAT_OPEN_TRIGGER.CLICK },
        );
      });

      it('provides commenting ranges', async () => {
        await showChatWithEditor();
        expect(mockCommentController.commentingRangeProvider).not.toBeUndefined();
      });
    });

    describe('with open options', () => {
      let customMessage: string;
      let customDocument: vscode.TextDocument;
      let customRange: vscode.Range;
      let openOptions: QuickChatOpenOptions;

      beforeEach(() => {
        customMessage = 'How do I computer?';
        customDocument = createMockTextDocument({
          uri: vscode.Uri.file('/custom/path/file.ts'),
          content: 'custom document content\ncustom second line\ncustom third line',
        });
        customRange = new vscode.Range(2, 0, 2, 12);

        openOptions = {
          trigger: QUICK_CHAT_OPEN_TRIGGER.CLICK,
          message: customMessage,
          document: customDocument,
          range: customRange,
        };

        // Setup active editor for tests that don't use the context
        const mockEditor = createFakePartial<vscode.TextEditor>({
          document: createFakePartial<vscode.TextDocument>({
            uri: vscode.Uri.file('/active/editor/file.ts'),
          }),
          selection: new vscode.Range(0, 0, 0, 0),
          setDecorations: jest.fn(),
        });
        asMutable(vscode.window).activeTextEditor = mockEditor;
      });

      it('uses document and range from open options when provided', async () => {
        (mockCommentController.createCommentThread as jest.Mock).mockReturnValue(mockThread);

        await quickChat.show(openOptions);

        expect(mockCommentController.createCommentThread).toHaveBeenCalledWith(
          customDocument.uri,
          customRange,
          [],
        );
      });

      it('passes open options to send method', async () => {
        jest.spyOn(quickChat, 'send');
        (mockCommentController.createCommentThread as jest.Mock).mockReturnValue(mockThread);

        await quickChat.show(openOptions);

        expect(quickChat.send).toHaveBeenCalledWith(expect.any(Object), openOptions);
      });

      it('uses provided message instead of prompting user', async () => {
        jest.spyOn(quickChat, 'send');
        (mockCommentController.createCommentThread as jest.Mock).mockReturnValue(mockThread);

        await quickChat.show(openOptions);

        expect(vscode.window.showInputBox).not.toHaveBeenCalled();
        expect(quickChat.send).toHaveBeenCalledWith(
          { text: customMessage, thread: mockThread },
          openOptions,
        );
      });
    });
  });

  describe('send', () => {
    let mockReply: vscode.CommentReply;
    let updateHandler: jest.Mock;

    describe('with no open options', () => {
      beforeEach(() => {
        mockReply = createFakePartial<vscode.CommentReply>({
          thread: mockThread,
          text: 'Test question',
        });

        updateHandler = jest.fn();
        jest.mocked(mockApi.subscribeToUpdates).mockImplementation(handler => {
          updateHandler.mockImplementation(handler);
          return Promise.resolve({} as Cable);
        });

        (utils.createComment as jest.Mock).mockImplementation(text => ({ body: text }));

        return showChatWithEditor();
      });

      it('build context from active editor document and selection', async () => {
        await quickChat.send(mockReply);

        expect(mockApi.processNewUserPrompt).toHaveBeenCalledWith(
          'Test question',
          expect.any(String),
          expect.objectContaining({
            fileName: '/path/to/file.ts',
            selectedText: 'second line',
          }),
        );
      });

      it('handles full response correctly', async () => {
        const responseText = 'Full response text';
        await updateHandler({ content: responseText });

        expect(mockThread.comments[2].body).toEqual(
          expect.objectContaining({ value: responseText }),
        );
      });

      it('handles chunked responses correctly', async () => {
        const mockAppendMarkdown = jest.fn();
        const mockMarkdown = {
          appendMarkdown: mockAppendMarkdown,
          value: '',
        };
        (vscode.MarkdownString as jest.Mock) = jest.fn(() => mockMarkdown);

        await quickChat.send(mockReply);
        await updateHandler({ chunkId: 1, content: 'Chunk 1 ' });
        await updateHandler({ chunkId: 2, content: 'Chunk 2 ' });

        expect(mockAppendMarkdown).toHaveBeenNthCalledWith(1, 'Chunk 1 ');
        expect(mockAppendMarkdown).toHaveBeenNthCalledWith(2, 'Chunk 2 ');
      });

      it('handles /clear command correctly', async () => {
        mockReply.text = SPECIAL_MESSAGES.CLEAR;
        await quickChat.send(mockReply);

        expect(mockApi.clearChat).toHaveBeenCalled();
        expect(utils.setLoadingContext).toHaveBeenCalledWith(false);
        expect(mockThread.comments).toEqual([]);
      });

      it('handles /reset command correctly', async () => {
        jest
          .mocked(utils.createChatResetComment)
          .mockReturnValue(createFakePartial<vscode.Comment>({ body: 'reset' }));

        mockReply.text = SPECIAL_MESSAGES.RESET;
        await quickChat.send(mockReply);

        expect(mockApi.resetChat).toHaveBeenCalled();
        expect(utils.setLoadingContext).toHaveBeenCalledWith(false);
        expect(mockThread.comments).toContainEqual({ body: 'reset' });
      });

      it('tracks telemetry for sending messages', async () => {
        const userInput = 'Test message';
        mockReply.text = userInput;

        await quickChat.send(mockReply);

        expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
          COMMAND_QUICK_CHAT_MESSAGE_TELEMETRY,
          { message: userInput },
        );
      });
    });

    describe('with open options', () => {
      let customDocument: vscode.TextDocument;
      let customRange: vscode.Range;
      let openOptions: QuickChatOpenOptionsWithSelection;

      beforeEach(async () => {
        customDocument = createMockTextDocument({
          uri: vscode.Uri.file('/custom/file.ts'),
          content: 'custom document content\ncustom second line\ncustom third line',
        });
        customRange = new vscode.Range(2, 0, 2, 12);

        openOptions = {
          trigger: QUICK_CHAT_OPEN_TRIGGER.CLICK,
          document: customDocument,
          range: customRange,
        };

        mockReply = createFakePartial<vscode.CommentReply>({
          thread: mockThread,
          text: 'Test question',
        });

        await showChatWithEditor();
        asMutable(vscode.workspace).asRelativePath = jest
          .fn()
          .mockReturnValue(customDocument.uri.path);
        jest.clearAllMocks();
      });

      it('builds file context from provided document and range', async () => {
        await quickChat.send(mockReply, openOptions);

        expect(mockApi.processNewUserPrompt).toHaveBeenCalledWith(
          'Test question',
          expect.any(String),
          expect.objectContaining({
            fileName: '/custom/file.ts',
            selectedText: 'custom third',
          }),
        );
      });
    });

    describe('with no open options or active editor document', () => {
      it('sends undefined file context', async () => {
        asMutable(vscode.window).activeTextEditor = undefined;
        await quickChat.send(mockReply, {
          trigger: QUICK_CHAT_OPEN_TRIGGER.CLICK,
        });

        const expectedContext = undefined;
        expect(mockApi.processNewUserPrompt).toHaveBeenCalledWith(
          'Test question',
          expect.any(String),
          expectedContext,
        );
      });
    });
  });

  describe('Quick Actions', () => {
    it('registers a completion item provider for quick actions', () => {
      expect(vscode.languages.registerCompletionItemProvider).toHaveBeenCalledWith(
        { scheme: 'comment', pattern: '**' },
        { provideCompletionItems: utils.provideCompletionItems },
        '/',
      );
    });
  });

  describe('thread label update', () => {
    const mockSelection = createFakePartial<vscode.Selection>({
      active: new vscode.Position(0, 0),
      start: new vscode.Position(0, 0),
      end: new vscode.Position(0, 0),
    });
    const mockChangeEvent = createFakePartial<vscode.TextEditorSelectionChangeEvent>({
      textEditor: {
        document: {
          lineAt: jest.fn(),
          uri: {
            scheme: 'file',
            authority: 'other',
          },
        },
        selection: mockSelection,
        setDecorations: jest.fn(),
      },
    });

    const originalLabel = 'Original label';

    beforeEach(async () => {
      mockThread = createFakePartial<vscode.CommentThread>({
        label: originalLabel,
        range: new vscode.Range(0, 0, 0, 0),
      });

      await showChatWithEditor();
      jest.useFakeTimers();
    });

    it('should update the thread label on selection change', async () => {
      const mockLabel = 'New selection for quick chat';
      jest.mocked(generateThreadLabel).mockReturnValue(mockLabel);

      await triggerTextEditorSelectionChange(mockChangeEvent);

      jest.runAllTimers();

      expect(generateThreadLabel).toHaveBeenCalled();
      expect(mockDebug).toHaveBeenCalledTimes(1);
      expect(mockThread.label).toBe(mockLabel);
    });

    it('does not update thread label when selection is un-changed', async () => {
      jest.mocked(generateThreadLabel).mockReturnValue(originalLabel);
      await triggerTextEditorSelectionChange(mockChangeEvent);

      jest.runAllTimers();
      mockDebug.mockClear();

      await triggerTextEditorSelectionChange(mockChangeEvent);

      jest.runAllTimers();
      expect(generateThreadLabel).toHaveBeenCalled();
      expect(mockDebug).not.toHaveBeenCalled();
      expect(mockThread.label).toBe(originalLabel);
    });

    it('should not update the label when the thread comment is in focus', () => {
      jest.mocked(generateThreadLabel).mockClear();
      triggerTextEditorSelectionChange(
        createFakePartial<vscode.TextEditorSelectionChangeEvent>({
          textEditor: {
            document: {
              lineAt: jest.fn(),
              uri: {
                scheme: 'comment',
                authority: COMMENT_CONTROLLER_ID,
              },
            },
            selection: mockSelection,
          },
        }),
      );
      expect(generateThreadLabel).not.toHaveBeenCalled();
    });
  });

  describe('keybinding hints', () => {
    let mockEditor: vscode.TextEditor;
    let mockDecorationType: vscode.TextEditorDecorationType;
    const triggerConfigurationChange = createConfigurationChangeTrigger();
    const cursorLineRange = new vscode.Range(0, 0, 3, 5);

    beforeEach(() => {
      mockEditor = createFakePartial<vscode.TextEditor>({
        selection: createFakePartial<vscode.Selection>({ active: new vscode.Position(0, 0) }),
        document: createFakePartial<vscode.TextDocument>({
          lineAt: jest.fn().mockReturnValue({ isEmptyOrWhitespace: true, range: cursorLineRange }),
          uri: vscode.Uri.file('/test/file.ts'), // Default to a file URI
        }),
        setDecorations: jest.fn(),
      });

      mockDecorationType = createFakePartial<vscode.TextEditorDecorationType>({
        dispose: jest.fn(),
      });
      jest.mocked(utils.createKeybindingHintDecoration).mockClear();
      jest.mocked(utils.createKeybindingHintDecoration).mockReturnValue(mockDecorationType);
      jest.useFakeTimers();
      createChat();
    });

    const triggerSelectionChange = () =>
      triggerTextEditorSelectionChange({
        textEditor: mockEditor,
      } as vscode.TextEditorSelectionChangeEvent);

    it('creates keybinding decoration only once', async () => {
      expect(utils.createKeybindingHintDecoration).toHaveBeenCalledTimes(1);
      await triggerSelectionChange();

      jest.runAllTimers();
      expect(utils.createKeybindingHintDecoration).toHaveBeenCalledTimes(1);
    });

    it('shows hint when enabled and on empty line with correct range', async () => {
      expect(utils.createKeybindingHintDecoration).toHaveBeenCalled();
      await triggerSelectionChange();

      jest.runAllTimers();

      expect(mockEditor.setDecorations).toHaveBeenCalledWith(mockDecorationType, []);
      expect(mockEditor.setDecorations).toHaveBeenCalledWith(mockDecorationType, [cursorLineRange]);
    });

    it('does not show hint when disabled', async () => {
      mockConfig.keybindingHints.enabled = false;
      await triggerConfigurationChange();
      await triggerSelectionChange();
      jest.runAllTimers();

      expect(mockEditor.setDecorations).toHaveBeenCalledWith(mockDecorationType, []);
      expect(mockEditor.setDecorations).not.toHaveBeenCalledWith(mockDecorationType, [
        cursorLineRange,
      ]);
    });

    it('does not show hint on non-empty lines', async () => {
      mockEditor.document.lineAt = jest.fn().mockReturnValue({ isEmptyOrWhitespace: false });
      await triggerSelectionChange();
      jest.runAllTimers();

      expect(mockEditor.setDecorations).toHaveBeenCalledWith(mockDecorationType, []);
      expect(mockEditor.setDecorations).not.toHaveBeenCalledWith(mockDecorationType, [
        cursorLineRange,
      ]);
    });

    it('does not show hint for non-file documents', async () => {
      mockEditor = createFakePartial<vscode.TextEditor>({
        selection: createFakePartial<vscode.Selection>({ active: new vscode.Position(0, 0) }),
        document: createFakePartial<vscode.TextDocument>({
          lineAt: jest.fn().mockReturnValue({ isEmptyOrWhitespace: true }),
          uri: vscode.Uri.parse('output:test'), // Create a non-file URI
        }),
        setDecorations: jest.fn(),
      });

      await triggerSelectionChange();
      jest.runAllTimers();

      expect(mockEditor.setDecorations).toHaveBeenCalledWith(mockDecorationType, []);
      expect(mockEditor.setDecorations).not.toHaveBeenCalledWith(mockDecorationType, [
        cursorLineRange,
      ]);
    });
  });

  describe('gutter icon', () => {
    let mockDecorationType: vscode.TextEditorDecorationType;

    beforeEach(async () => {
      mockDecorationType = createFakePartial<vscode.TextEditorDecorationType>({
        dispose: jest.fn(),
      });
      jest.mocked(utils.createGutterIconDecoration).mockReturnValue(mockDecorationType);
    });

    it('shows gutter icon when chat is shown', async () => {
      const { mockEditor } = await showChatWithEditor();

      expect(mockEditor.setDecorations).toHaveBeenCalledWith(mockDecorationType, [
        { range: new vscode.Range(mockThread.range.end, mockThread.range.end) },
      ]);
    });

    it('removes gutter icon when thread is collapsed', async () => {
      const { mockEditor } = await showChatWithEditor();
      mockThread.collapsibleState = vscode.CommentThreadCollapsibleState.Collapsed;

      jest
        .mocked(vscode.window.onDidChangeTextEditorVisibleRanges)
        .mock.calls.forEach(([listener]) =>
          listener(
            createFakePartial<vscode.TextEditorVisibleRangesChangeEvent>({
              textEditor: mockEditor,
            }),
          ),
        );

      expect(mockDecorationType.dispose).toHaveBeenCalled();
    });

    it('shows gutter icon when in comment input', async () => {
      const { mockEditor } = await showChatWithEditor();

      // Create new mock editor with comment input URI
      const commentInputEditor = createFakePartial<vscode.TextEditor>({
        document: {
          uri: createFakePartial<vscode.Uri>({
            authority: COMMENT_CONTROLLER_ID,
            scheme: 'comment',
          }),
        },
        selection: new vscode.Selection(0, 0, 0, 0),
        setDecorations: jest.fn(),
      });

      asMutable(vscode.window).visibleTextEditors = [mockEditor];
      asMutable(vscode.window).activeTextEditor = commentInputEditor;

      jest
        .mocked(vscode.window.onDidChangeTextEditorVisibleRanges)
        .mock.calls.forEach(([listener]) =>
          listener(
            createFakePartial<vscode.TextEditorVisibleRangesChangeEvent>({
              textEditor: commentInputEditor,
            }),
          ),
        );

      expect(mockEditor.setDecorations).toHaveBeenCalledWith(mockDecorationType, [
        { range: new vscode.Range(mockThread.range.end, mockThread.range.end) },
      ]);
    });

    it('does not show gutter icon for different documents', async () => {
      await showChatWithEditor();

      const differentEditor = createFakePartial<vscode.TextEditor>({
        document: { uri: vscode.Uri.file('/different.ts') },
        selection: new vscode.Selection(0, 0, 0, 0),
        setDecorations: jest.fn(),
      });
      asMutable(vscode.window).activeTextEditor = differentEditor;

      jest
        .mocked(vscode.window.onDidChangeTextEditorVisibleRanges)
        .mock.calls.forEach(([listener]) =>
          listener(
            createFakePartial<vscode.TextEditorVisibleRangesChangeEvent>({
              textEditor: differentEditor,
            }),
          ),
        );

      expect(differentEditor.setDecorations).not.toHaveBeenCalledWith(
        mockDecorationType,
        expect.any(Array),
      );
    });
  });

  describe('document changes', () => {
    beforeEach(async () => {
      mockThread = createFakePartial<vscode.CommentThread>({
        uri: vscode.Uri.file('/test.ts'),
        range: new vscode.Range(0, 0, 5, 10),
      });

      await showChatWithEditor();
    });

    it('updates thread position when text changes', () => {
      const change = {
        range: new vscode.Range(2, 0, 2, 0),
        text: 'new line\n',
      };

      const newRange = new vscode.Range(0, 0, 6, 10);
      jest.mocked(utils.calculateThreadRange).mockReturnValue(newRange);

      expect(mockThread.range.end.line).toBe(5);

      jest.mocked(vscode.workspace.onDidChangeTextDocument).mock.calls.forEach(([listener]) =>
        listener(
          createFakePartial<vscode.TextDocumentChangeEvent>({
            document: createFakePartial<vscode.TextDocument>({ uri: mockThread.uri }),
            contentChanges: [change],
          }),
        ),
      );

      expect(mockThread.range.end.line).toBe(6);
    });
  });

  describe('set chat visibility state', () => {
    it.each`
      message         | isExpanded | quickChatOpen
      ${'showing'}    | ${true}    | ${true}
      ${'collapsing'} | ${false}   | ${false}
    `(
      '$message quickChat updates the key to $quickChatOpen',
      async ({ isExpanded, quickChatOpen }) => {
        (mockCommentController.createCommentThread as jest.Mock).mockReturnValue(mockThread);

        const { mockEditor } = await showChatWithEditor();
        mockThread.collapsibleState = isExpanded
          ? vscode.CommentThreadCollapsibleState.Expanded
          : vscode.CommentThreadCollapsibleState.Collapsed;

        jest
          .mocked(vscode.window.onDidChangeTextEditorVisibleRanges)
          .mock.calls.forEach(([listener]) =>
            listener(
              createFakePartial<vscode.TextEditorVisibleRangesChangeEvent>({
                textEditor: mockEditor,
              }),
            ),
          );

        expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
          'setContext',
          'gitlab:quickChatOpen',
          quickChatOpen,
        );
      },
    );

    it('updates context key to false if activeEditor document uri is not quickChat thread uri', async () => {
      (mockCommentController.createCommentThread as jest.Mock).mockReturnValue(mockThread);
      await quickChat.show();

      jest.mocked(vscode.window.onDidChangeActiveTextEditor).mock.calls.forEach(([listener]) =>
        listener(
          createFakePartial<vscode.TextEditor>({
            document: { uri: vscode.Uri.file('/diffFile/path') },
          }),
        ),
      );

      expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
        'setContext',
        'gitlab:quickChatOpen',
        false,
      );
    });

    it('updates context key to true if activeEditor document uri is quickChat thread uri', async () => {
      const diffUri = vscode.Uri.file('/diffFile/path');
      mockThread = createFakePartial<vscode.CommentThread>({
        uri: diffUri,
        range: new vscode.Range(0, 0, 1, 0),
        collapsibleState: vscode.CommentThreadCollapsibleState.Expanded,
      });
      (mockCommentController.createCommentThread as jest.Mock).mockReturnValue(mockThread);

      await quickChat.show();

      jest.mocked(vscode.window.onDidChangeActiveTextEditor).mock.calls.forEach(([listener]) =>
        listener(
          createFakePartial<vscode.TextEditor>({
            document: { uri: diffUri },
          }),
        ),
      );

      expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
        'setContext',
        'gitlab:quickChatOpen',
        true,
      );
    });

    it('hides keybinding hint when chat is open', async () => {
      const diffUri = vscode.Uri.file('/diffFile/path');
      mockThread = createFakePartial<vscode.CommentThread>({
        uri: diffUri,
        range: new vscode.Range(0, 0, 1, 0),
        collapsibleState: vscode.CommentThreadCollapsibleState.Expanded,
      });
      (mockCommentController.createCommentThread as jest.Mock).mockReturnValue(mockThread);
      const { mockEditor } = await showChatWithEditor();

      jest.mocked(vscode.window.onDidChangeActiveTextEditor).mock.calls.forEach(([listener]) =>
        listener(
          createFakePartial<vscode.TextEditor>({
            document: { uri: diffUri },
          }),
        ),
      );
      await Promise.resolve();

      const setDecorationCalls = jest.mocked(mockEditor.setDecorations).mock.calls;

      expect(setDecorationCalls.length).toBe(1);
      expect(setDecorationCalls[0][1]).toEqual([]);
    });
  });
});
