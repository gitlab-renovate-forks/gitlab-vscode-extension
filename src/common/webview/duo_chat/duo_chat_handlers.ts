import * as vscode from 'vscode';
import { log } from '../../log';
import { DUO_CHAT_WEBVIEW_ID } from '../../constants';
import { getActiveFileContext } from '../../chat/gitlab_chat_file_context';
import { WebviewMessageRegistry } from '../message_handlers/webview_message_registry';
import { insertCodeSnippet as insertCodeSnippetFn } from '../../chat/insert_code_snippet';
import { copyCodeSnippet as copyCodeSnippetFn } from '../../chat/copy_code_snippet';
import { LSDuoChatWebviewController } from './duo_chat_controller';

interface SnippetActionParams {
  snippet: string;
}

export const insertCodeSnippet = async (params: SnippetActionParams | unknown) => {
  const { snippet } = params as SnippetActionParams;
  if (snippet) {
    await insertCodeSnippetFn(snippet);
  }
};

export const copyCodeSnippet = async (params: SnippetActionParams | unknown) => {
  const { snippet } = params as SnippetActionParams;
  if (snippet) {
    await copyCodeSnippetFn(snippet);
  }
};
export type MessageType = 'error' | 'warning' | 'info';

interface UserMessageParams {
  message: string;
  type: MessageType;
}

export const showUserMessage = async (params: UserMessageParams | unknown) => {
  const { type, message } = params as UserMessageParams;

  switch (type) {
    case 'error':
      await vscode.window.showErrorMessage(message);
      break;
    case 'warning':
      await vscode.window.showWarningMessage(message);
      break;
    case 'info':
      await vscode.window.showInformationMessage(message);
      break;
    default:
      log.warn(`Unsupported notification type: ${type}`);
  }
};

interface FocusStateParams {
  isFocused: boolean;
}

export const setChatFocusState = async (params: FocusStateParams | unknown) => {
  const { isFocused } = params as FocusStateParams;
  await vscode.commands.executeCommand('setContext', 'gitlab:chatFocused', isFocused);
};

interface OpenLinkParams {
  href: string;
}

export const openLink = async (params: OpenLinkParams | unknown) => {
  const { href } = params as OpenLinkParams;

  try {
    if (!href) return;

    const uri = vscode.Uri.parse(href);
    const result = await vscode.env.openExternal(uri);

    if (!result) {
      throw new Error(`Failed to open URL: ${href}`);
    }
  } catch (e) {
    log.warn(e.message);
  }
};

export const registerDuoChatHandlers = (
  registry: WebviewMessageRegistry,
  controller: LSDuoChatWebviewController,
) => {
  registry.onRequest(DUO_CHAT_WEBVIEW_ID, 'getCurrentFileContext', getActiveFileContext);

  registry.onNotification(DUO_CHAT_WEBVIEW_ID, 'insertCodeSnippet', insertCodeSnippet);
  registry.onNotification(DUO_CHAT_WEBVIEW_ID, 'copyCodeSnippet', copyCodeSnippet);
  registry.onNotification(DUO_CHAT_WEBVIEW_ID, 'showMessage', showUserMessage);
  registry.onNotification(DUO_CHAT_WEBVIEW_ID, 'focusChange', setChatFocusState);
  registry.onNotification(DUO_CHAT_WEBVIEW_ID, 'appReady', () => controller.setChatReady());
  registry.onNotification(DUO_CHAT_WEBVIEW_ID, 'openLink', openLink);
};
