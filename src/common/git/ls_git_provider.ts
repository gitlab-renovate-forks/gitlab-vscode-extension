import * as vscode from 'vscode';

export interface LSGitProvider {
  getDiffWithHead(repositoryUri: vscode.Uri): Promise<string | undefined>;
  getDiffWithBranch(repositoryUri: vscode.Uri, branch: string): Promise<string | undefined>;
}

export class DefaultLSGitProvider implements LSGitProvider {
  async getDiffWithHead(): Promise<string | undefined> {
    throw new Error('Not implemented');
  }

  async getDiffWithBranch(): Promise<string | undefined> {
    throw new Error('Not implemented');
  }
}
