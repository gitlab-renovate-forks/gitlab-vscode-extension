import {
  CHAT,
  CHAT_NO_LICENSE,
  CODE_SUGGESTIONS,
  DUO_DISABLED_FOR_PROJECT,
  FeatureState,
} from '@gitlab-org/gitlab-lsp';
import vscode from 'vscode';
import { createFakePartial } from '../test_utils/create_fake_partial';
import { UNSUPPORTED_LANGUAGE } from '../code_suggestions/state_policy/language_server_policy';
import { FeatureStateManager } from '../language_server/feature_state_manager';
import { ChatState, ChatStateManager } from './chat_state_manager';

const waitForPromises = () => new Promise(process.nextTick);
describe('Chat State Manager', () => {
  let chatStataManager: ChatStateManager;

  let triggerOnChange: (params: FeatureState[]) => void;
  const mockOnChange = jest.fn();
  const mockOnChangeDisposable = jest.fn();
  const featureStateManager = createFakePartial<FeatureStateManager>({
    onChange: mockOnChange,
  });

  beforeEach(async () => {
    mockOnChange.mockImplementation(_callback => {
      triggerOnChange = _callback;
      return { dispose: mockOnChangeDisposable };
    });
    chatStataManager = new ChatStateManager(featureStateManager);
  });

  describe('Chat License check', () => {
    it('should run command to disable chat when user has no Duo Chat License', async () => {
      const chatDisabledParams = createFakePartial<FeatureState[]>([
        {
          featureId: CHAT,
          engagedChecks: [
            {
              checkId: CHAT_NO_LICENSE,
            },
          ],
        },
      ]);

      triggerOnChange(chatDisabledParams);
      await waitForPromises();
      expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
        'setContext',
        'gitlab:chatAvailable',
        false,
      );
    });

    it('should run command to enable chat when Duo Chat license check is not engaged', async () => {
      const chatEnabledParams = createFakePartial<FeatureState[]>([
        {
          featureId: CODE_SUGGESTIONS,
          engagedChecks: [
            {
              checkId: UNSUPPORTED_LANGUAGE,
            },
          ],
        },
        {
          featureId: CHAT,
          engagedChecks: [],
        },
      ]);

      triggerOnChange(chatEnabledParams);
      await waitForPromises();
      expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
        'setContext',
        'gitlab:chatAvailable',
        true,
      );
    });
  });

  describe('Duo enabled for project check', () => {
    it('should run command to disable chat when Duo is disabled for project', async () => {
      const chatDisabledParams = createFakePartial<FeatureState[]>([
        {
          featureId: CHAT,
          engagedChecks: [
            {
              checkId: DUO_DISABLED_FOR_PROJECT,
            },
          ],
        },
      ]);

      triggerOnChange(chatDisabledParams);
      await waitForPromises();
      expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
        'setContext',
        'gitlab:chatAvailableForProject',
        false,
      );
    });

    it('should run command to enable chat when Duo enabled for project', async () => {
      const chatEnabledParams = createFakePartial<FeatureState[]>([
        {
          featureId: CODE_SUGGESTIONS,
          engagedChecks: [
            {
              checkId: UNSUPPORTED_LANGUAGE,
            },
          ],
        },
        {
          featureId: CHAT,
          engagedChecks: [],
        },
      ]);

      triggerOnChange(chatEnabledParams);
      await waitForPromises();
      expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
        'setContext',
        'gitlab:chatAvailableForProject',
        true,
      );
    });
  });

  it('should call "onChange" listener with the new state when state is updated', async () => {
    const listener = jest.fn();

    chatStataManager.onChange(listener);
    const mockStates: FeatureState[] = [
      { featureId: CHAT, engagedChecks: [] },
      { featureId: CODE_SUGGESTIONS, engagedChecks: [] },
    ];
    triggerOnChange(mockStates);
    await waitForPromises();
    expect(listener).toHaveBeenCalledWith({
      chatAvailable: true,
    } satisfies ChatState);
  });
});
