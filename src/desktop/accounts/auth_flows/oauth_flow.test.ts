import assert from 'assert';
import vscode from 'vscode';
import { ExchangeTokenResponse, GitLabService } from '../../gitlab/gitlab_service';
import { GitLabUriHandler } from '../../gitlab_uri_handler';
import { openUrl } from '../../commands/openers';
import { createFakeFetchFromApi } from '../../../common/test_utils/create_fake_fetch_from_api';
import { currentUserRequest } from '../../../common/gitlab/api/get_current_user';
import { doNotAwait } from '../../../common/utils/do_not_await';
import { GITLAB_COM_URL } from '../../../common/constants';
import { Credentials } from '../../../common/platform/gitlab_account';
import { OAuthFlow } from './oauth_flow';

jest.mock('../../commands/openers');
jest.mock('../../gitlab/gitlab_service');
jest.useFakeTimers();

/* This method simulates the first response from GitLab OAuth, it accepts the authentication URL and returns redirect URL */
const fakeOAuthService = (urlString: string): string => {
  const url = new URL(urlString);
  const params = url.searchParams;
  assert.strictEqual(
    params.get('client_id'),
    '36f2a70cddeb5a0889d4fd8295c241b7e9848e89cf9e599d0eed2d8e5350fbf5',
  );
  assert.strictEqual(params.get('redirect_uri'), 'vscode://gitlab.gitlab-workflow/authentication');
  assert.strictEqual(params.get('response_type'), 'code');
  assert.strictEqual(params.get('scope'), 'api');
  assert.strictEqual(params.get('code_challenge_method'), 'S256');
  assert(params.get('state'));
  assert(params.get('code_challenge'));

  const responseParams = new URLSearchParams({ state: params.get('state') || '', code: 'abc' });
  return `${params.get('redirect_uri')}?${responseParams}`;
};

describe('OAuthFlow', () => {
  let uriHandler: GitLabUriHandler;

  beforeEach(async () => {
    uriHandler = new GitLabUriHandler();
    const exchangeToken: ExchangeTokenResponse = {
      access_token: 'test_token',
      expires_in: 7200,
      refresh_token: '**********',
      created_at: 0,
    };

    jest.mocked(GitLabService.exchangeToken).mockResolvedValue(exchangeToken);
    jest.mocked(GitLabService).mockImplementation(({ instanceUrl, token }: Credentials) => {
      assert.strictEqual(instanceUrl, 'https://gitlab.com');
      assert.strictEqual(token, 'test_token');
      return {
        fetchFromApi: createFakeFetchFromApi({
          request: currentUserRequest,
          response: { id: 123, username: 'test_user' },
        }),
      } as GitLabService;
    });
  });

  describe('authenticate', () => {
    describe('when user authenticates the request', () => {
      beforeEach(() => {
        jest.mocked(openUrl).mockImplementationOnce(async urlString => {
          uriHandler.fire(vscode.Uri.parse(fakeOAuthService(urlString)));
        });
      });

      it('authenticates', async () => {
        const flow = new OAuthFlow(uriHandler);

        const account = await flow.authenticate(GITLAB_COM_URL);

        expect(account?.id).toEqual('https://gitlab.com|123');
        expect(account?.token).toEqual('test_token');
        expect(vscode.window.withProgress).toHaveBeenCalledWith(
          {
            title: 'Waiting for OAuth redirect from GitLab.com.',
            location: vscode.ProgressLocation.Notification,
          },
          expect.any(Function),
        );
      });
    });

    describe('when user does not authenticate the request', () => {
      beforeEach(() => {
        jest.mocked(openUrl).mockImplementation(async () => {
          /* noop */
        });
      });

      it('cancels OAuth login after 60s', async () => {
        const flow = new OAuthFlow(uriHandler);

        const resultPromise = flow.authenticate(GITLAB_COM_URL);

        // if we await the result this function call, the rejected promise fails the test before we reach the assertion ¯\_(ツ)_/¯
        // I do not know why that's happening, but after an hour of looking into fake timers implementation I give up
        // I verified that the test fails if the production code timeout doesn't work.
        // if you want to investigate for yourself, start here https://stackoverflow.com/a/51132058/606571
        doNotAwait(jest.advanceTimersByTimeAsync(61000));

        await expect(resultPromise).rejects.toThrow(/Cancelling the GitLab OAuth login after 60s/);
      });
    });
  });
});
