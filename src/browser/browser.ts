import * as vscode from 'vscode';
import { initializeLogging } from '../common/log';
import { activateCommon } from '../common/main';
import {
  FeatureFlag,
  getLocalFeatureFlagService,
} from '../common/feature_flags/local_feature_flag_service';
import { LanguageServerManager } from '../common/language_server/language_server_manager';
import { CodeSuggestions } from '../common/code_suggestions/code_suggestions';
import { setupWebviews, WebviewMessageRegistry } from '../common/webview';
import { WEB_IDE_EXTENSION_ID, WebIDEExtension } from '../common/platform/web_ide';
import { runExtensionConfigurationMigrations } from '../common/utils/extension_configuration_migrations/migrations';
import { AIContextManagerWebIde } from '../common/chat/ai_context_manager_web_ide';
import { FeatureStateManager } from '../common/language_server/feature_state_manager';
import { DefaultDiagnosticsService } from '../common/diagnostics/diagnostics_service';
import { DefaultExtensionStateService } from '../common/state/extension_state_service';
import { VersionStateProvider } from '../common/state/version_state_provider';
import {
  VersionDetailsStateKey,
  VersionDiagnosticsRenderer,
} from '../common/diagnostics/version_diagnostics/version_diagnostics_renderer';
import { GitLabInstanceVersionProvider } from '../common/state/gitlab_instance_version_provider';
import { browserLanguageClientFactory } from './language_server/browser_language_client_factory';
import { createDependencyContainer } from './dependency_container_browser';

export const activate = async (context: vscode.ExtensionContext) => {
  const webIdeExtension =
    vscode.extensions.getExtension<WebIDEExtension>(WEB_IDE_EXTENSION_ID)?.exports;

  if (!webIdeExtension) {
    throw new Error(`Failed to load extension export from ${WEB_IDE_EXTENSION_ID}.`);
  }

  const outputChannel = vscode.window.createOutputChannel('GitLab Workflow');
  initializeLogging(line => outputChannel.appendLine(line));

  await runExtensionConfigurationMigrations();
  const dependencyContainer = await createDependencyContainer(webIdeExtension);

  // browser always has account linked and repo opened.
  await vscode.commands.executeCommand('setContext', 'gitlab:noAccount', false);
  await vscode.commands.executeCommand('setContext', 'gitlab:validState', true);

  // TODO: integrate language server into web IDE duo chat
  const aiContextManager = new AIContextManagerWebIde();
  let featureStateManager: FeatureStateManager | undefined;
  const extensionStateService = new DefaultExtensionStateService();
  const diagnosticsService = new DefaultDiagnosticsService(extensionStateService);

  await activateCommon(
    context,
    dependencyContainer,
    outputChannel,
    aiContextManager,
    diagnosticsService,
    featureStateManager,
  );

  let languageServerManager: LanguageServerManager | undefined;

  if (getLocalFeatureFlagService().isEnabled(FeatureFlag.LanguageServerWebIDE)) {
    const webviewMessageRegistry = new WebviewMessageRegistry();
    featureStateManager = new FeatureStateManager();
    languageServerManager = new LanguageServerManager(
      context,
      browserLanguageClientFactory,
      dependencyContainer,
      webviewMessageRegistry,
      featureStateManager,
    );
    await languageServerManager.startLanguageServer();
    context.subscriptions.push(await setupWebviews(languageServerManager, webviewMessageRegistry));
  } else {
    const codeSuggestions = new CodeSuggestions(context, dependencyContainer.gitLabPlatformManager);
    await codeSuggestions.init();
    context.subscriptions.push(codeSuggestions);
  }

  const glStateProvider = new GitLabInstanceVersionProvider(
    dependencyContainer.gitLabPlatformManager,
  );

  extensionStateService.addStateProvider(
    VersionDetailsStateKey,
    new VersionStateProvider(
      context.extension.packageJSON.version,
      languageServerManager,
      glStateProvider,
    ),
  );
  diagnosticsService.addRenderer(new VersionDiagnosticsRenderer());

  await activateCommon(
    context,
    dependencyContainer,
    outputChannel,
    aiContextManager,
    diagnosticsService,
    featureStateManager,
  );
};
